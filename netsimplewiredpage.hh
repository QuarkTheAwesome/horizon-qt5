#ifndef NETWORKSIMPLEWIREDPAGE_HH
#define NETWORKSIMPLEWIREDPAGE_HH

#include "horizonwizardpage.hh"

#include <QLabel>
#include <QThread>
#include <QProgressBar>

class NetworkSimpleWiredPage : public HorizonWizardPage {
public:
	NetworkSimpleWiredPage(QWidget *parent = 0);
protected:
	void initializePage();
	bool isComplete() const;
private:
	bool complete = false;
	void setComplete(bool complete);

	QLabel *statusLabel, *descLabel;
	QProgressBar *progressBar;
	void updateStatusLabel(std::string iface, std::string method);

	bool tryInitInterface(Horizon::NetworkInterface iface);
	void onInitFail();
	void onInitOk();
};

#endif /* NETWORKSIMPLEWIREDPAGE_HH */

