#include "welcomepage.hh"

#include <QLabel>
#include <QVBoxLayout>

WelcomePage::WelcomePage(QWidget *parent) : HorizonWizardPage(parent)
{
        QLabel *descLabel;
        QVBoxLayout *layout;

        loadWatermark("welcome");
        setTitle(tr("Welcome to Adélie Linux"));

#ifdef NOT_NATIVE
        descLabel = new QLabel(
                    tr("<p>"
                       "Horizon will guide you through creation of a basic "
                       "<code>installfile</code> "
                       "for installing Adélie Linux on another computer."
                       "<p>"
                       "<b>IMPORTANT:</b> Not all advanced settings will "
                       "be available to you.  You may be allowed to "
                       "specify an invalid or non-bootable disk layout or "
                       "network configuration.  For best results, always "
                       "run System Installation directly on the computer "
                       "you wish to run Adélie Linux."
                       "<p>"
                       "For more information about the "
                       "<code>installfile</code> "
                       "format and syntax, see the "
                       "<a href='http://docs.adelielinux.org/'>"
                       "Adélie Linux Installation Guide</a> on the "
                       "Internet."));
        descLabel->setOpenExternalLinks(true);
        descLabel->setTextFormat(Qt::RichText);
#else
        descLabel = new QLabel(
                    tr("The streamlined installation process for Adélie "
                       "Linux will only take about 10-15 minutes of your "
                       "time.  After you're done, your computer will be "
                       "running the reliable, secure, libre Adélie Linux "
                       "operating system.\n\n"

                       "When you're ready to answer a few questions, get "
                       "started by choosing Next.  If you'd like more "
                       "information about the installation procedure, "
                       "choose Help at any time."));
#endif
        descLabel->setWordWrap(true);

        layout = new QVBoxLayout;
        layout->addWidget(descLabel);
        setLayout(layout);
}

int WelcomePage::nextId() const
{
#ifndef NOT_NATIVE
        if(this->horizonWizard()->interfaces.size() > 0)
        {
#endif
                return HorizonWizard::Page_Networking;
#ifndef NOT_NATIVE
        }
        else
        {
                return HorizonWizard::Page_Software;
        }
#endif
}
