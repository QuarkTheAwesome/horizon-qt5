#include "networkingpage.hh"
#include "horizonwizard.hh"

#include <cstdint>
#include <QLabel>
#include <QVBoxLayout>

NetworkingPage::NetworkingPage(QWidget *parent) : HorizonWizardPage(parent)
{
        QLabel *descLabel;
        QVBoxLayout *layout;

        loadWatermark("network");
        setTitle(tr("Networking Setup"));

        descLabel = new QLabel(tr(
                    "If you have a normal network connection where your "
                    "computer is directly connected to the Internet via "
                    "Ethernet or Wi-Fi using a modem or router, choose "
                    "Simple.  If you need to set a static IP address, "
                    "or you use a VPN or proxy server, choose Advanced.\n\n"

                    "If you don't want to configure networking or you "
                    "don't want to use this computer on the Internet, "
                    "choose Skip."));
        descLabel->setWordWrap(true);

        simple = new QRadioButton(tr(
                 "&Simple - my computer connects to the Internet directly\n"
                 "or via a modem/router"));
        advanced = new QRadioButton(tr(
                   "&Advanced - my computer connects to an enterprise "
                   "network\nor I use a static IP address, VPN, or 802.1X"));
        skip = new QRadioButton(tr(
                   "S&kip - I don't want to connect to a network or the "
                   "Internet right now"));

        radioGroup = new QButtonGroup(this);
        radioGroup->addButton(simple);
        radioGroup->addButton(advanced);
        radioGroup->addButton(skip);

        QObject::connect(radioGroup, (void (QButtonGroup:: *)(QAbstractButton *))&QButtonGroup::buttonClicked,
                         [=](QAbstractButton *button) {
                emit completeChanged();
        });

        layout = new QVBoxLayout;
        layout->addWidget(descLabel);
        layout->addSpacing(50);
        layout->addWidget(simple);
        layout->addWidget(advanced);
        layout->addWidget(skip);
        setLayout(layout);
}

bool NetworkingPage::isComplete() const
{
        return (radioGroup->checkedButton() != nullptr);
}

int NetworkingPage::nextId() const
{
        if(radioGroup->checkedButton() == simple)
        {
                bool tryWireless = false;

                for(auto &iface : this->horizonWizard()->interfaces)
                {
                        if(iface.is_wireless())
                        {
                                tryWireless = true;
                                break;
                        }
                }

                if(tryWireless)
                {
                        return HorizonWizard::Page_Network_SimpleWireless;
                }
                else
                {
                        return HorizonWizard::Page_Network_SimpleWired;
                }
        }
        else if(radioGroup->checkedButton() == advanced)
        {
                return HorizonWizard::Page_Network_Advanced;
        }
        else
        {
                return HorizonWizard::Page_Software;
        }
}
