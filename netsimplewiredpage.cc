#include "netsimplewiredpage.hh"

#include <QVBoxLayout>
#include <QLabel>
#include <QProgressBar>
#include <QDebug>
#include <QtConcurrent>

using std::string;
using Horizon::NetworkInterface;

/* Timeout for getting a DHCP lease, seconds */
#define DHCP_TIMEOUT 10
/* Text to represent the DHCP protocol, used in GUI */
#define DHCP_STR "DHCP"

NetworkSimpleWiredPage::NetworkSimpleWiredPage(QWidget* parent)
: HorizonWizardPage(parent) {
	QVBoxLayout *layout;

	/* Mostly boring GUI stuff ahead, worth noting that a lot of the
	 * components are globals. Should probably be using signals instead
	 * of doing this - not sure what the Qt way would be */

	loadWatermark("network");
	setTitle(tr("Wired Networking Setup"));

	//TODO this text
	descLabel = new QLabel(tr(
		"Setup will now attempt to set up a network " \
		"on your computer. Please wait..."));
	descLabel->setWordWrap(true);

	statusLabel = new QLabel(tr("Please wait..."));

	progressBar = new QProgressBar;
	progressBar->setRange(0, 0);

	layout = new QVBoxLayout;
	layout->addWidget(descLabel);
	layout->addStretch(0);
	layout->addWidget(statusLabel);
	layout->addWidget(progressBar);
	setLayout(layout);

	/* Mark the page as incomplete, which disables the Next button */
	setComplete(false);
}

void NetworkSimpleWiredPage::initializePage() {
	/* Start setting up interfaces right away... on another thread 
	 * (don't want to lock up the GUI) */
	QtConcurrent::run([this]() {
		/* For every connected, non-wireless interface... */
		for (NetworkInterface &iface : horizonWizard()->interfaces) {
			if (iface.is_connected() && !iface.is_wireless()) {
				/* try to set it up */
				bool success = tryInitInterface(iface);
				/* If that worked, call it quits.
				 * Otherwise, the loop will do the next iface */
				if (success) {
					onInitOk();
					return;
				}
			}
		}
		/* If we get here, the loop tried every interface and none of
		 * them worked... set some GUI stuff to tell the user */
		onInitFail();
	});
}

void NetworkSimpleWiredPage::onInitFail() {
	//TODO make this a little slicker, add some actual fail text
	statusLabel->setVisible(false);
	descLabel->setText(tr("Failed"));
}

void NetworkSimpleWiredPage::onInitOk() {
	/* Move to the next wizard page
	 * (this is a little weird since we're on a different thread) 
	 * The Next button doesn't need to be enabled for this to work */
	QMetaObject::invokeMethod(this->wizard(), "next", Qt::QueuedConnection);
}

bool NetworkSimpleWiredPage::tryInitInterface(NetworkInterface iface) {
	QSemaphore *sem = new QSemaphore;
	bool dhcpOk;

	/* It'd be really nice to not lock up this thread waiting for dhcp, but
	 * I just couldn't make it work... open to ideas. */

	/* Keep the user updated on what we're doing */
	updateStatusLabel(iface.device_name(), DHCP_STR);
	/* Attempt to get a DHCP lease on the given interface */
	iface.try_dhcp(DHCP_TIMEOUT, [sem, &dhcpOk](bool success, string log_path, void* privdata) {
		(void) log_path; (void) privdata; (void) success;

		//FIXME we're simulating an async operation for now
		QtConcurrent::run([sem, &dhcpOk]() {
			QThread::sleep(3);
			dhcpOk = true; //false to simulate failure
			sem->release();
		});
	});

	/* block until the callback runs sem->release() */
	sem->acquire();
	return dhcpOk;
}

void NetworkSimpleWiredPage::updateStatusLabel(string iface, string method) {
	statusLabel->setText(
		tr(("Setting up " + iface + " (" + method + ")").c_str()));
}

/* Wrappers for isComplete, allowing us to enable/disable the Next button */

bool NetworkSimpleWiredPage::isComplete() const {
	return complete;
}

void NetworkSimpleWiredPage::setComplete(bool complete) {
	this->complete = complete;
	emit completeChanged();
}
