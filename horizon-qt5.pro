QT += widgets \
      concurrent

CONFIG += c++11

TARGET = horizon-qt5

TEMPLATE = app

SOURCES += main.cc \
    horizonwizard.cc \
    welcomepage.cc \
    networkingpage.cc \
    horizonwizardpage.cc \
    softwarepage.cc \
    horizonhelpwindow.cc \
    netsimplewifipage.cc \
    netsimplewiredpage.cc

HEADERS += \
    horizonwizard.hh \
    welcomepage.hh \
    networkingpage.hh \
    horizonwizardpage.hh \
    softwarepage.hh \
    horizonhelpwindow.hh \
    netsimplewifipage.hh \
    netsimplewiredpage.hh

RESOURCES += \
    horizon.qrc

unix: CONFIG += link_pkgconfig
unix: PKG_CONFIG = PKG_CONFIG_PATH=/usr/local/lib/pkgconfig pkgconf
unix: PKGCONFIG += horizon-core
