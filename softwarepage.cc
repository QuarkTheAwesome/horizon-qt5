#include "softwarepage.hh"
#include "horizonwizard.hh"

#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>

SoftwarePage::SoftwarePage(QWidget *parent) : HorizonWizardPage(parent)
{
        QLabel *descLabel;
        QVBoxLayout *layout, *desktopLayout;
        QHBoxLayout *nudgeOver;

        loadWatermark("software");
        setTitle(tr("Select Software"));

        descLabel = new QLabel(tr(
                    "Select the software you want to install on your "
                    "computer.  You can add more later."));
        descLabel->setWordWrap(true);

        desktop = new QCheckBox(tr(
                  "&Desktop Software – browser, email client, media "
                  "player, and more"));
        kde = new QCheckBox(tr(
              "Use the &KDE Plasma 5 desktop\nIdeal for newer computers "
              "with a clean, modern design."));
        lxqt = new QCheckBox(tr(
               "Use the &LXQt desktop\nOptimised for older computers with a "
               "design similar to Plasma."));
        xfce = new QCheckBox(tr(
               "Use the &XFCE 4 desktop\nProvides a classic design "
               "suitable for smaller screens."));
        openbox = new QCheckBox(tr(
                  "Use the &OpenBox desktop\nFor those familiar with other "
                  "Linux/Unix desktops."));
        server = new QCheckBox(tr(
                 "&Server Software – Web server, print server, and more"));
        advanced = new QCheckBox(tr(
                   "&Advanced – select which software to install"));

        QObject::connect(desktop, (void (QCheckBox:: *)(bool))&QCheckBox::clicked,
                         [=](bool checked) {
                this->toggleDesktops(checked);
        });

        desktop->setChecked(true);
        kde->setChecked(true);

        typeGroup = new QButtonGroup(this);
        typeGroup->addButton(kde);
        typeGroup->addButton(lxqt);
        typeGroup->addButton(xfce);
        typeGroup->addButton(openbox);

        desktopLayout = new QVBoxLayout;
        desktopLayout->addWidget(kde);
        desktopLayout->addWidget(lxqt);
        desktopLayout->addWidget(xfce);
        desktopLayout->addWidget(openbox);

        nudgeOver = new QHBoxLayout;
        nudgeOver->addSpacing(20);
        nudgeOver->addLayout(desktopLayout);

        layout = new QVBoxLayout;
        layout->addWidget(descLabel);
        layout->addSpacing(50);
        layout->addWidget(desktop);
        layout->addLayout(nudgeOver);
        layout->addWidget(server);
        layout->addWidget(advanced);
        setLayout(layout);
}

void SoftwarePage::toggleDesktops(bool show)
{
        kde->setVisible(show);
        xfce->setVisible(show);
        lxqt->setVisible(show);
        openbox->setVisible(show);
}

int SoftwarePage::nextId() const
{
        if(advanced->isChecked())
        {
                return HorizonWizard::Page_AdvancedSoftware;
        } else {
                return HorizonWizard::Page_Startup;
        }
}
