#ifndef HORIZONWIZARD_HH
#define HORIZONWIZARD_HH

#include <QWizard>
#include <set>
#include <string>
#include <vector>

#include <horizon/networkinterface.hh>

class HorizonWizard : public QWizard
{
public:
        enum { Page_Welcome, /* introduction */
               Page_Partition, /* partitioning */
               Page_Networking, /* network type selection */
               Page_Network_SimpleWireless, /* simple -> wifi */
               Page_Network_SimpleWired, /* simple -> wired */
               Page_Network_Advanced, /* advanced network config */
               Page_Software, /* software selection */
               Page_AdvancedSoftware, /* advanced software selection */
               Page_Startup, /* boot loader config, if arch needed */
               Page_Commit, /* confirm committing to disk */
               Page_Save, /* save the installfile */
               Page_Finished, /* done */
               Page_CustomStart /* custom pages have IDs starting here */
        };

        HorizonWizard(QWidget *parent = 0);
        std::set<std::string> selected;
        std::vector<Horizon::NetworkInterface> interfaces;
};

#endif // HORIZONWIZARD_HH
