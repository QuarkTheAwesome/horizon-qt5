#include "horizonwizard.hh"
#include "horizonhelpwindow.hh"

#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <map>
#include <string>

#include "welcomepage.hh"
#ifndef NOT_NATIVE
#       include <horizon/networkinterface.hh>
using Horizon::NetworkInterface;
#endif
#include "networkingpage.hh"
#include "netsimplewifipage.hh"
#include "netsimplewiredpage.hh"
#include "softwarepage.hh"

using std::map;
using std::string;

map<int, string> help_id_map = {
        {HorizonWizard::Page_Welcome, "welcome"},
        {HorizonWizard::Page_Networking, "network-start"},
        {HorizonWizard::Page_Network_SimpleWireless, "network-wifi"},
        {HorizonWizard::Page_Network_SimpleWired, "network-wired"},
        {HorizonWizard::Page_Network_Advanced, "network-advanced"},
        {HorizonWizard::Page_Software, "software"},
        {HorizonWizard::Page_AdvancedSoftware, "software-advanced"},
        {HorizonWizard::Page_Startup, "startup"},
        {HorizonWizard::Page_Commit, "commit"},
        {HorizonWizard::Page_Save, "save"},
        {HorizonWizard::Page_Finished, "finished"}
};

HorizonWizard::HorizonWizard(QWidget *parent) : QWizard(parent)
{
        setWindowTitle(tr("Adélie Linux System Installation"));

        setFixedSize(QSize(650, 450));

        setOption(DisabledBackButtonOnLastPage);
        setOption(HaveHelpButton);
        setOption(NoCancelButton);

        setSizeGripEnabled(false);

        setPage(Page_Welcome, new WelcomePage);
        setPage(Page_Networking, new NetworkingPage);
        setPage(Page_Network_SimpleWireless, new NetworkSimpleWirelessPage);
        setPage(Page_Network_SimpleWired, new NetworkSimpleWiredPage);
        setPage(Page_Software, new SoftwarePage);

        QObject::connect(this, (void (QWizard:: *)(void))&QWizard::helpRequested,
                         [=](void) {
                if(help_id_map.find(currentId()) == help_id_map.end())
                {
                        qDebug() << "no help available for " << currentId();
                        QMessageBox nohelp(QMessageBox::Warning,
                                           tr("No Help Available"),
                                           tr("Help is not available for the current page.  Consult the Installation Guide for more information."),
                                           QMessageBox::Ok,
                                           this);
                        nohelp.exec();
                        return;
                }
                string helppath = ":/wizard_help/resources/" +
                                  help_id_map.at(currentId()) + "-help.txt";
                QFile helpfile(helppath.c_str());
                helpfile.open(QFile::ReadOnly);
                HorizonHelpWindow help(&helpfile, this);
                help.exec();
        });

        this->selected.insert("adelie-base");
#ifndef NOT_NATIVE
        this->interfaces = NetworkInterface::list_available_ifs();
#endif
}
