#ifndef WELCOMEPAGE_HH
#define WELCOMEPAGE_HH

#include "horizonwizardpage.hh"

class WelcomePage : public HorizonWizardPage
{
public:
        WelcomePage(QWidget *parent = 0);
        int nextId() const;
};

#endif // WELCOMEPAGE_HH
